﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GM : MonoBehaviour
{
    public bool level_1;
    public bool level_2;
    public bool level_3;
    public bool level_4;
    public bool level_5;
    public bool level_6;
    public bool q_pic;

    public RawImage mainpic_1;
    public RawImage mainpic_2;
    public Texture pic_1;
    public Texture pic_2;
    public Texture pic_3;
    public Texture pic_4;
    public Texture pic_5;
    public Texture pic_6;
    public Texture pic_7;
    public Texture pic_8;
    public Texture pic_9;
    public Texture pic_10;
    public Texture pic_11;
    public Texture pic_12;
    public Texture pic_13;
    public Texture pic_14;
    public Texture pic_15;
    public GameObject hammer;
    public GameObject fertilizer;
    public GameObject seed;
    public GameObject Sprinklers;
    public GameObject hand;
    public GameObject vege;

    public Material gd_m;
    public Texture ogg;
    public Texture dirt;
    Animator ani;

    int once;
    int once_1;

    public GameObject pos;
    public GameObject pos_nobug;
    public GameObject vegepic;

    public GameObject loadcan;
    AsyncOperation async;
    float asyncnum;
    public Text percent;

    // Start is called before the first frame update
    void Start()
    {
        ran_pic(Random.Range(1,16));
        gd_m.mainTexture = ogg;
        ani = hand.GetComponent<Animator>();

        for (int x = 0; x<=5;x++)
        {
            GameObject pos_cn = Instantiate(pos);
            pos_cn.transform.position = new Vector3(1f + x, -0.53f, -12f);
            pos_cn.transform.SetParent(vege.transform);
            for (int y = 1; y <=5; y++)
            {
                GameObject pos1_cn = Instantiate(pos);
                pos1_cn.transform.position = new Vector3(1f+x, -0.53f, -12f-y);
                pos1_cn.transform.SetParent(vege.transform);
            }
        }
        for (int x = 0; x <= 17; x++)
        {
            GameObject vegepic_cn = Instantiate(pos_nobug);
            vegepic_cn.transform.position = new Vector3(-5f + x, -0.53f, 8f);
            vegepic_cn.transform.SetParent(vege.transform);
            for (int y = 1; y <= 47; y++)
            {
                GameObject vegepic_1_cn = Instantiate(pos_nobug);
                vegepic_1_cn.transform.position = new Vector3(-5f + x, -0.53f, 8f - y);
                vegepic_1_cn.transform.SetParent(vege.transform);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Return) && once == 0)
        {
            this.GetComponent<AudioSource>().enabled = true;
            once = 1;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            loadcan.SetActive(true);
            async = SceneManager.LoadSceneAsync(0);
            async.allowSceneActivation = false;
        }
        if (loadcan.activeSelf)
        {
            print(async.progress);
            asyncnum = async.progress * 100;
            percent.text = asyncnum.ToString("0");
            if (async.progress >= 0.9f)
            {
                async.allowSceneActivation = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (level_1)
        {
            hammer.SetActive(true);
            ani.Play("hand_hold_stick");
        }
        if (level_2)
        {
            gd_m.mainTexture = dirt;
            hammer.SetActive(false);
            fertilizer.SetActive(true);
            ani.Play("hand_hold_lantern");
        }
        if (level_3)
        {
            fertilizer.SetActive(false);
            seed.SetActive(true);
        }
        if (level_4)
        {
            seed.SetActive(false);
            Sprinklers.SetActive(true);
            vege.SetActive(true);
            ani.Play("hand_bow_string_grasp");
        }
        if (level_5 && once_1 == 0)
        {
            Sprinklers.SetActive(false);
            ani.Play("idle");
            once_1 = 1;
        }
        if (q_pic)
        {
            if (this.GetComponent<AudioSource>().volume > 0)
            {
                this.GetComponent<AudioSource>().volume = this.GetComponent<AudioSource>().volume - 0.01f * Time.deltaTime;
            }
        }
    }
    void ran_pic(int xx)
    {
        if (xx == 1)
        {
            mainpic_1.texture = pic_1;
            mainpic_2.texture = pic_1;
        }
        if (xx == 2)
        {
            mainpic_1.texture = pic_2;
            mainpic_2.texture = pic_2;
        }
        if (xx == 3)
        {
            mainpic_1.texture = pic_3;
            mainpic_2.texture = pic_3;
        }
        if (xx == 4)
        {
            mainpic_1.texture = pic_4;
            mainpic_2.texture = pic_4;
        }
        if (xx == 5)
        {
            mainpic_1.texture = pic_5;
            mainpic_2.texture = pic_5;
        }
        if (xx == 6)
        {
            mainpic_1.texture = pic_6;
            mainpic_2.texture = pic_6;
        }
        if (xx == 7)
        {
            mainpic_1.texture = pic_7;
            mainpic_2.texture = pic_7;
        }
        if (xx == 8)
        {
            mainpic_1.texture = pic_8;
            mainpic_2.texture = pic_8;
        }
        if (xx == 9)
        {
            mainpic_1.texture = pic_9;
            mainpic_2.texture = pic_9;
        }
        if (xx == 10)
        {
            mainpic_1.texture = pic_10;
            mainpic_2.texture = pic_10;
        }
        if (xx == 11)
        {
            mainpic_1.texture = pic_11;
            mainpic_2.texture = pic_11;
        }
        if (xx == 12)
        {
            mainpic_1.texture = pic_12;
            mainpic_2.texture = pic_12;
        }
        if (xx == 13)
        {
            mainpic_1.texture = pic_13;
            mainpic_2.texture = pic_13;
        }
        if (xx == 14)
        {
            mainpic_1.texture = pic_14;
            mainpic_2.texture = pic_14;
        }
        if (xx == 15)
        {
            mainpic_1.texture = pic_15;
            mainpic_2.texture = pic_15;
        }
    }
}
