﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bug_sc : MonoBehaviour
{
    public bool catc;
    GameObject gm;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("gm");
    }

    // Update is called once per frame
    void Update()
    {
        if (gm.GetComponent<GM>().level_5 && catc == false && this.transform.localScale.x < 0.04f)
        {
            this.transform.localScale = new Vector3(this.transform.localScale.x + 0.02f * Time.deltaTime, this.transform.localScale.y + 0.02f * Time.deltaTime, this.transform.localScale.z + 0.02f * Time.deltaTime);
        }
        if (this.transform.localScale.x >= 0.04f && catc == false)
        {
            this.transform.localScale = new Vector3(0.04f, 0.04f, 0.04f);
            this.GetComponent<Animator>().enabled = true;
        }
        if (catc && this.transform.localScale.x > 0)
        {
            this.transform.localScale = new Vector3(this.transform.localScale.x - 0.02f * Time.deltaTime, this.transform.localScale.y - 0.02f * Time.deltaTime, this.transform.localScale.z - 0.02f * Time.deltaTime);
        }
        if (gm.GetComponent<GM>().level_6)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "hand" && catc == false)
        {
            this.GetComponent<AudioSource>().Play();
            catc = true;
            this.GetComponent<Animator>().enabled = false;
            print("bb");
        }
    }
}
