﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vege : MonoBehaviour
{
    bool catc;
    GameObject gm;
    public bool stop;
    public int once;

    public GameObject parti;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("gm");
        StartCoroutine(delete_vege());
    }

    // Update is called once per frame
    void Update()
    {
        if (catc && this.transform.localScale.x > 0)
        {
            this.transform.localScale = new Vector3(this.transform.localScale.x - 0.2f * Time.deltaTime, this.transform.localScale.y - 0.2f * Time.deltaTime, this.transform.localScale.z - 0.2f * Time.deltaTime);
        }
        if (gm.GetComponent<GM>().level_6)
        {
            stop = true;
        }
        if (gm.GetComponent<GM>().q_pic)
        {
            StartCoroutine(count());
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "hand" && catc == false && stop)
        {
            this.GetComponent<AudioSource>().Play();
            GameObject parti_cn = Instantiate(parti,this.transform.parent.gameObject.transform);
            parti_cn.transform.localPosition = new Vector3(0,0.027f,0);
            //parti_cn.transform.SetParent(transform);

            catc = true;
            print("bb");
        }
        if (other.tag == "vege" && once == 0)
        {
            print("aa");
            Destroy(this.gameObject);
        }
    }
    private IEnumerator count()
    {
        yield return new WaitForSeconds(6);
        stop = false;
    }
    private IEnumerator delete_vege()
    {
        yield return new WaitForSeconds(0.5f);
        once = 1;
    }
}
