﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class catch_sc : MonoBehaviour
{
    public bool once = false;
    GameObject gm;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("gm");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "bug" && once == false && gm.GetComponent<GM>().level_5)
        {
            this.GetComponent<Animator>().Play("catch");
            StartCoroutine(catchani());
            print("aa");
            once = true;
        }
        if (other.tag == "vege" && once == false && gm.GetComponent<GM>().level_6)
        {
            this.GetComponent<Animator>().Play("catch");
            StartCoroutine(catchani());
            once = true;
        }
    }
    private IEnumerator catchani()
    {
        yield return new WaitForSeconds(1);
        this.GetComponent<Animator>().Play("idle");
        once = false;
    }
}
