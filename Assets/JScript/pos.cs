﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pos : MonoBehaviour
{
    public GameObject vege_1;
    public GameObject vege_2;
    public GameObject vege_3;
    public GameObject vege;

    // Start is called before the first frame update
    void Start()
    {
        ran_vege(Random.Range(1,5));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void ran_vege(int xx)
    {
        if (xx == 1)
        {
            GameObject vege_cn = Instantiate(vege_1);
            vege_cn.transform.SetParent(this.transform);
            vege_cn.transform.localEulerAngles = new Vector3(0, Random.Range(0,360), 0);
            vege_cn.transform.localPosition = new Vector3(0,0,0);
            vege_cn.transform.localScale = new Vector3(0.1f,0.1f,0.1f);

        }
        if (xx == 2)
        {
            GameObject vege_cn = Instantiate(vege_2);
            vege_cn.transform.SetParent(this.transform);
            vege_cn.transform.localEulerAngles = new Vector3(0, Random.Range(0, 360), 0);
            vege_cn.transform.localPosition = new Vector3(0, 0, 0);
            vege_cn.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }
        if (xx == 3)
        {
            GameObject vege_cn = Instantiate(vege_3);
            vege_cn.transform.SetParent(this.transform);
            vege_cn.transform.localEulerAngles = new Vector3(0, Random.Range(0, 360), 0);
            vege_cn.transform.localPosition = new Vector3(0, 0, 0);
            vege_cn.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }
        if (xx == 4)
        {
            GameObject vege_cn = Instantiate(vege);
            vege_cn.transform.SetParent(this.transform);
            vege_cn.transform.localEulerAngles = new Vector3(0, Random.Range(0, 360), 0);
            vege_cn.transform.localPosition = new Vector3(0, -0.03f, 0);
            vege_cn.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }
    }
}
