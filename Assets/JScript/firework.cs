﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class firework : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(firework_fn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private IEnumerator firework_fn()
    {
        yield return new WaitForSeconds(2);
        Destroy(this.gameObject);
    }
}
