﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIsc : MonoBehaviour
{
    public GameObject front;
    public GameObject lv_1;
    public GameObject lv_2;
    public GameObject lv_3;
    public GameObject lv_4;
    public GameObject lv_5;
    public GameObject lv_6;
    public GameObject q_pic;
    public GameObject gm;

    int once;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && once == 0)
        {
            front.SetActive(true);
            StartCoroutine(rundow());
            once = 1;
        }
    }
    private IEnumerator rundow()
    {
        yield return new WaitForSeconds(5);
        front.SetActive(false);
        lv_1.SetActive(true);
        gm.GetComponent<GM>().level_1 = true;
        yield return new WaitForSeconds(10);
        lv_1.SetActive(false);
        lv_2.SetActive(true);
        gm.GetComponent<GM>().level_1 = false;
        gm.GetComponent<GM>().level_2 = true;
        yield return new WaitForSeconds(10);
        lv_2.SetActive(false);
        lv_3.SetActive(true);
        gm.GetComponent<GM>().level_2 = false;
        gm.GetComponent<GM>().level_3 = true;
        yield return new WaitForSeconds(10);
        lv_3.SetActive(false);
        lv_4.SetActive(true);
        gm.GetComponent<GM>().level_3 = false;
        gm.GetComponent<GM>().level_4 = true;
        yield return new WaitForSeconds(10);
        lv_4.SetActive(false);
        lv_5.SetActive(true);
        gm.GetComponent<GM>().level_4 = false;
        gm.GetComponent<GM>().level_5 = true;
        yield return new WaitForSeconds(18);
        lv_5.SetActive(false);
        lv_6.SetActive(true);
        gm.GetComponent<GM>().level_5 = false;
        gm.GetComponent<GM>().level_6 = true;
        yield return new WaitForSeconds(10);
        lv_6.SetActive(false);
        q_pic.SetActive(true);
        gm.GetComponent<GM>().level_6 = false;
        gm.GetComponent<GM>().q_pic = true;
    }
}
