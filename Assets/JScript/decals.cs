﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class decals : MonoBehaviour
{
    public GameObject deca;
    public int once;
    public Transform hand;
    Vector3 worldPos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        worldPos = hand.TransformPoint(hand.transform.localPosition );
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ground" && once == 0)
        {
            GameObject deca_cn = Instantiate(deca, worldPos, Quaternion.FromToRotation(Vector3.up, other.transform.up));
            deca_cn.transform.position = new Vector3(deca_cn.transform.position.x,-0.48f, deca_cn.transform.position.z);
            //deca_cn.transform.position = new Vector3(deca_cn.transform.localPosition.x, deca_cn.transform.localPosition.y, deca_cn.transform.localPosition.z);
            this.GetComponent<AudioSource>().Play();
            Destroy(deca_cn,10f);
            print("aa");
            once = 1;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ground" && once == 1)
        {
            print("bb");
            once = 0;
        }
    }
}
